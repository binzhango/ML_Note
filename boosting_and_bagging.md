[ML DOC](https://ml-cheatsheet.readthedocs.io/en/latest/index.html#)

1）Bagging + 决策树 = 随机森林

2）AdaBoost + 决策树 = 提升树

3）Gradient Boosting + 决策树 = GBDT

[approaching-almost-any-machine-learning-problem](https://blog.kaggle.com/2016/07/21/approaching-almost-any-machine-learning-problem-abhishek-thakur/)
